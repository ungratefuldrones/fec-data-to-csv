import csv
import click

# Legend http://www.fec.gov/finance/disclosure/metadata/DataDictionaryOperatingExpenditures.shtml

expenditure_fieldnames = [
	'CMTE_ID', 
	'AMNDT_IND', 
	'RPT_YR', 
	'RPT_TP', 
	'IMAGE_NUM', 
	'LINE_NUM',
	'FORM_TP_CD', 
	'SCHED_TP_CD', 
	'NAME', 
	'CITY', 
	'STATE', 
	'ZIP_CODE',
	'TRANSACTION_DT', 
	'TRANSACTION_AMT', 
	'TRANSACTION_PGI', 
	'PURPOSE', 
	'CATEGORY', 
	'CATEGORY_DESC',
	'MEMO_CD', 
	'MEMO_TEXT', 
	'ENTITY_TP', 
	'SUB_ID', 
	'FILE_NUM', 
	'TRAN_ID',
	'BACK_REF_TRAN_ID'
]

committee_fieldnames = [
	'CMTE_ID',
	'CMTE_NM',
	'TRES_NM',
	'CMTE_ST1',
	'CMTE_ST2',
	'CMTE_CITY',
	'CMTE_ST',
	'CMTE_ZIP',
	'CMTE_DSGN',
	'CMTE_TP',
	'CMTE_PTY_AFFILIATION',
	'CMTE_FILING_FREQ',
	'ORG_TP',
	'CONNECTED_ORG_NM',
	'CAND_ID'
]

candidate_fieldnames = [
	'CAND_ID',
	'CAND_NAME',
	'CAND_PTY_AFFILIATION',
	'CAND_ELECTION_YR',
	'CAND_OFFICE_ST',
	'CAND_OFFICE',
	'CAND_OFFICE_DISTRICT',
	'CAND_ICI',
	'CAND_STATUS',
	'CAND_PCC',
	'CAND_ST1',
	'CAND_ST2',
	'CAND_CITY',
	'CAND_ST',
	'CAND_ZIP'
]


def parse_txt( file_path ):
	data = open(file_path).read().splitlines()
	l = []
	for line in data:
		l.append(line.split('|'))
	return l

@click.command()
@click.option('--input', prompt='Input file', type=click.Path() )
@click.option('--output', prompt='Output file', type=click.Path() )
@click.option('--type', prompt='Type', type=click.Choice(['expenditure', 'committee', 'candidate']) )
def process( input, output, type ):
	data = parse_txt( input )

	with open( output , 'w') as csvfile:
		if (type == 'expenditure'):
			fieldnames = expenditure_fieldnames
		elif ( type == 'committee' ):
			fieldnames =  committee_fieldnames
		elif ( type == 'candidate' ):
			fieldnames = candidate_fieldnames
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for d in data:
			writer.writerow( { y: d[x] for x, y in enumerate(fieldnames) })

if __name__ == '__main__':
    process()


